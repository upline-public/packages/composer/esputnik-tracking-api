<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;


class GeneralInfo implements Arrayable
{
    private string $siteId;
    private int $datetime;
    private string $scCookie;

    private ?string $userPhone = null;
    private ?string $userEmail = null;
    private ?string $userName = null;
    private ?string $userEsContactId = null;


    public function __construct(
        string $siteId,
        int    $datetime,
        string $scCookie
    )
    {
        $this->siteId = $siteId;
        $this->datetime = $datetime;
        $this->scCookie = $scCookie;
    }

    /**
     * @param string|null $userPhone
     * @return GeneralInfo
     */
    public function setUserPhone(?string $userPhone): GeneralInfo
    {
        $this->userPhone = $userPhone;
        return $this;
    }

    /**
     * @param string|null $userEmail
     * @return GeneralInfo
     */
    public function setUserEmail(?string $userEmail): GeneralInfo
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @param string|null $userName
     * @return GeneralInfo
     */
    public function setUserName(?string $userName): GeneralInfo
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @param string|null $userEsContactId
     * @return GeneralInfo
     */
    public function setUserEsContactId(?string $userEsContactId): GeneralInfo
    {
        $this->userEsContactId = $userEsContactId;
        return $this;
    }


    public function toArray(): array
    {
        $data = [
            'siteId' => $this->siteId,
            'user_phone' => $this->userPhone,
            'user_email' => $this->userEmail,
            'user_name' => $this->userName,
            'user_es_contact_id' => $this->userEsContactId,
            'cookies' => [
                'sc' => $this->scCookie
            ],
            'datetime' => $this->datetime,
        ];

        return array_filter($data, fn($value) => !is_null($value));
    }
}
