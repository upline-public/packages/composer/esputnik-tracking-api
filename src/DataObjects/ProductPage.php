<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

use Uplinestudio\EsputnikTrackingApi\Traits\FieldTaggableTrait;
use Uplinestudio\EsputnikTrackingApi\Traits\TaggableTrait;

class ProductPage implements EventData
{
    use TaggableTrait;
    use FieldTaggableTrait;

    private const TAG_FIELD_PREFIX = 'tag_';
    private const EVENT_NAME = 'ProductPage';

    private string $productKey;
    private ?string $price = null;
    private ?bool $isInStock = null;


    public function __construct(string $productKey)
    {
        $this->productKey = $productKey;
    }

    /**
     * @param  string|null  $price
     * @return ProductPage
     */
    public function setPrice(?string $price): ProductPage
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @param  bool|null  $isInStock
     * @return ProductPage
     */
    public function setIsInStock(?bool $isInStock): ProductPage
    {
        $this->isInStock = $isInStock;
        return $this;
    }

    public function toArray(): array
    {
        return array_merge(
            [
                'Product' => $this->getProductArray(),
            ],
            $this->getTagsRepresentation()
        );
    }

    private function getProductBaseArray(): array
    {
        $result = [
            'productKey' => $this->productKey,
        ];

        if (!is_null($this->price)) {
            $result['price'] = $this->price;
        }

        if (!is_null($this->isInStock)) {
            $result['isInStock'] = $this->isInStock ? '1' : '0';
        }

        return $result;
    }

    private function getProductArray(): array
    {
        return array_merge(
            $this->getProductBaseArray(),
            $this->getTagFields()
        );
    }

    public static function getEventName(): string
    {
        return self::EVENT_NAME;
    }
}
