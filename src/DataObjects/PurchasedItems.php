<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

use Uplinestudio\EsputnikTrackingApi\Traits\TaggableTrait;

class PurchasedItems implements EventData
{
    use TaggableTrait;

    private const EVENT_NAME = 'PurchasedItems';

    private string $guid;
    private string $orderNumber;

    public function __construct(string $guid, string $orderNumber)
    {

        $this->guid = $guid;
        $this->orderNumber = $orderNumber;
    }

    public function toArray(): array
    {
        return array_merge(
            [
                'GUID' => $this->guid,
                'OrderNumber' => $this->orderNumber
            ],
            $this->getTagsRepresentation()
        );
    }

    public static function getEventName(): string
    {
        return self::EVENT_NAME;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }
}
