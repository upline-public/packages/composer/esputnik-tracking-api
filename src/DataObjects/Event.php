<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

class Event implements Arrayable
{
    private GeneralInfo $generalInfo;
    private EventData $eventData;

    public function __construct(GeneralInfo $generalInfo, EventData $eventData)
    {
        $this->generalInfo = $generalInfo;
        $this->eventData = $eventData;
    }


    public function toArray(): array
    {
        $eventName = $this->eventData::getEventName();
        return [
            'GeneralInfo' => array_merge($this->generalInfo->toArray(), [
                'eventName' => $eventName
            ]),
            $eventName => $this->eventData->toArray()
        ];
    }
}
