<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;


use Uplinestudio\EsputnikTrackingApi\Traits\FieldTaggableTrait;

class CartProduct implements Arrayable
{
    use FieldTaggableTrait;

    private const TAG_FIELD_PREFIX = 'tag_';
    private string $productKey;
    private string $price;
    private int $quantity;
    private ?string $priceCurrencyCode = null;
    private ?string $discount = null;

    public function __construct(string $productKey, string $price, int $quantity)
    {
        $this->productKey = $productKey;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function toArray(): array
    {
        $data = [
            'productKey' => $this->productKey,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'price_currency_code' => $this->priceCurrencyCode,
            'discount' => $this->discount,
        ];

        return array_filter($data, fn($value) => !is_null($value));
    }

    /**
     * @return string
     */
    public function getProductKey(): string
    {
        return $this->productKey;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getPriceCurrencyCode(): ?string
    {
        return $this->priceCurrencyCode;
    }

    /**
     * @return string|null
     */
    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    /**
     * @param string|null $priceCurrencyCode
     * @return CartProduct
     */
    public function setPriceCurrencyCode(?string $priceCurrencyCode): CartProduct
    {
        $this->priceCurrencyCode = $priceCurrencyCode;
        return $this;
    }

    /**
     * @param string|null $discount
     * @return CartProduct
     */
    public function setDiscount(?string $discount): CartProduct
    {
        $this->discount = $discount;
        return $this;
    }
}
