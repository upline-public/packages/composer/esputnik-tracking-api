<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

use Uplinestudio\EsputnikTrackingApi\Traits\TaggableTrait;

class CategoryPage implements EventData
{
    use TaggableTrait;

    private const EVENT_NAME = 'CategoryPage';
    private string $categoryKey;


    public function __construct(string $categoryKey)
    {
        $this->categoryKey = $categoryKey;
    }

    public function toArray(): array
    {
        return array_merge(
            [
                'Category' => [
                    'categoryKey' => $this->categoryKey
                ],
            ],
            $this->getTagsRepresentation()
        );
    }

    public static function getEventName(): string
    {
        return self::EVENT_NAME;
    }
}
