<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

use Uplinestudio\EsputnikTrackingApi\Traits\TaggableTrait;

class StatusCart implements EventData
{
    use TaggableTrait;

    private const EVENT_NAME = 'StatusCart';
    private string $guid;
    /**
     * @var CartProduct[]
     */
    private array $products = [];

    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    public function toArray(): array
    {
        return array_merge(
            [
                'GUID' => $this->guid,
                'Products' => $this->getProductsArray(),
            ],
            $this->getTagsRepresentation()
        );
    }

    public static function getEventName(): string
    {
        return self::EVENT_NAME;
    }

    public function getProductsArray(): array
    {
        $products = [];

        foreach ($this->products as $product) {
            $products[] = array_merge(
                $product->toArray(),
                $product->getTagFields()
            );

        }

        return $products;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @return CartProduct[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param  string  $guid
     * @return StatusCart
     */
    public function setGuid(string $guid): StatusCart
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @param  CartProduct[]  $products
     * @return StatusCart
     */
    public function setProducts(array $products): StatusCart
    {
        $this->products = $products;
        return $this;
    }
}
