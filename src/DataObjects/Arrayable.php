<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

interface Arrayable
{
    public function toArray(): array;
}
