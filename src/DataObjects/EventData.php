<?php

namespace Uplinestudio\EsputnikTrackingApi\DataObjects;

interface EventData extends Arrayable
{
    public static function getEventName(): string;
}
