<?php

namespace Uplinestudio\EsputnikTrackingApi\Traits;

trait TaggableTrait
{

    private array $tags = [];

    public function addTag(string $tagName, string $tagValue): self
    {
        if (!isset($this->tags[$tagName])) {
            $this->tags[$tagName] = [];
        }
        $this->tags[$tagName][] = $tagValue;
        return $this;
    }

    private function getTagsRepresentation(): array
    {
        if (!$this->tags) {
            return [];
        }
        return [
            'Tags' => $this->tags
        ];
    }
}
