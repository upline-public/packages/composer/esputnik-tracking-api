<?php

namespace Uplinestudio\EsputnikTrackingApi\Traits;

trait FieldTaggableTrait
{

    private array $tagFields = [];

    public function addTagField(string $fieldName, string $value): self
    {
        if (!isset($this->tagFields[$fieldName])) {
            $this->tagFields[$fieldName] = [];
        }
        $this->tagFields[$fieldName][] = $value;
        return $this;
    }

    public function getTagFields(): array
    {
        $result = [];
        foreach ($this->tagFields as $key => $value) {
            $result[self::TAG_FIELD_PREFIX . $key] = $value;
        }
        return $result;
    }
}