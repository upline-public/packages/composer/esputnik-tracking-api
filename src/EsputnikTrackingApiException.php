<?php

namespace Uplinestudio\EsputnikTrackingApi;


use Throwable;

class EsputnikTrackingApiException extends \Exception
{
private  $request;
    private $response;

    public function __construct($request = null, $response = null, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        $this->request = $request;
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
