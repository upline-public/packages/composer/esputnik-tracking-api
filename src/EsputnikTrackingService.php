<?php

namespace Uplinestudio\EsputnikTrackingApi;

use JsonSerializable;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Throwable;
use Uplinestudio\EsputnikTrackingApi\DataObjects\Arrayable;
use Uplinestudio\EsputnikTrackingApi\DataObjects\Event;

class EsputnikTrackingService
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $httpFactory;
    private const SERVICE_URI = 'https://tracker.esputnik.com/api/v2';
    private StreamFactoryInterface $streamFactory;

    public function __construct(
        ClientInterface $client,
        RequestFactoryInterface $factory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->httpClient = $client;
        $this->httpFactory = $factory;
        $this->streamFactory = $streamFactory;
    }

    /**
     * @param  Arrayable  $data
     * @return RequestInterface
     */
    private function getRequest(Arrayable $data): RequestInterface
    {
        return $this->httpFactory
            ->createRequest('POST', self::SERVICE_URI)
            ->withBody($this->streamFactory->createStream(json_encode($data->toArray())));
    }

    /**
     * @param  Event  $event
     * @return void
     * @throws EsputnikTrackingApiException
     */
    public function registerEvent(Event $event)
    {

        $request = $this->getRequest($event);
        try {
            $response = $this->httpClient->sendRequest($request);
            if ($response->getStatusCode() !== 204) {
                throw new EsputnikTrackingApiException($request, $response, 'Response is wrong');
            }

        } catch (Throwable $exception) {
            if ($exception instanceof EsputnikTrackingApiException) {
                throw $exception;
            }
            throw new EsputnikTrackingApiException(
                $request,
                null,
                'Error during request',
                0,
                $exception->getPrevious()
            );
        }
    }
}
