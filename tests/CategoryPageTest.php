<?php


use Uplinestudio\EsputnikTrackingApi\DataObjects\Event;
use Uplinestudio\EsputnikTrackingApi\DataObjects\CategoryPage;
use PHPUnit\Framework\TestCase;
use Uplinestudio\EsputnikTrackingApi\DataObjects\GeneralInfo;

class CategoryPageTest extends TestCase
{

    public function testToArray()
    {
        $event = new Event(
            (
            new GeneralInfo(
                '8A412DC',
                1579622183208,
                'CDA68358-94FB-4D83-9655-3FEB3C4114A3'
            )
            )
                ->setUserPhone('1-541-754-3010')
                ->setUserEmail('user@mail.com')
                ->setUserName('Gregori Boczynski')
                ->setUserEsContactId('255830499'),
            (new CategoryPage('Для малышей'))
                ->addTag('some_tags', '1')
                ->addTag('some_tags', 'a2')
                ->addTag('some_tags1', '4')
                ->addTag('some_tags1', 'gg')
        );

        $expectedJson = '{
  "GeneralInfo": {
    "eventName": "CategoryPage",
    "siteId": "8A412DC",
    "datetime":1579622183208,
    "user_phone": "1-541-754-3010",
    "user_email": "user@mail.com",
    "user_name": "Gregori Boczynski",
    "user_es_contact_id": "255830499",
    "cookies": {
      "sc": "CDA68358-94FB-4D83-9655-3FEB3C4114A3"
    }
  },
  "CategoryPage": {
    "Category": {
      "categoryKey": "Для малышей"
    },
    "Tags": {
      "some_tags": [
        "1",
        "a2"
      ],
      "some_tags1": [
        "4",
        "gg"
      ]
    }
  }
}';
        $this->assertEquals($event->toArray(), json_decode($expectedJson, true));
    }
}
