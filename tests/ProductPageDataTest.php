<?php

class ProductPageDataTest extends \PHPUnit\Framework\TestCase
{
    public function testToArray()
    {
        $event = new \Uplinestudio\EsputnikTrackingApi\DataObjects\Event(
            (
            new \Uplinestudio\EsputnikTrackingApi\DataObjects\GeneralInfo(
                '8A412DC',
                1579622183208,
                'CDA68358-94FB-4D83-9655-3FEB3C4114A3'
            )
            )
                ->setUserPhone('1-541-754-3010')
                ->setUserEmail('user@mail.com')
                ->setUserName('Gregori Boczynski')
                ->setUserEsContactId('255830499'),
            (new \Uplinestudio\EsputnikTrackingApi\DataObjects\ProductPage(
                '72354',
            ))
                ->setPrice('754 USD')
                ->setIsInStock(true)
                ->addTagField('something', 'abc')
                ->addTagField('something', 'bca')
                ->addTag('some_tags', 'some_tag1')
                ->addTag('some_tags', 'some_tag2')
        );


        $expectedJson = '{
  "GeneralInfo": {
    "eventName": "ProductPage",
    "siteId": "8A412DC",
    "datetime":1579622183208,
    "user_phone": "1-541-754-3010",
    "user_email": "user@mail.com",
    "user_name": "Gregori Boczynski",
    "user_es_contact_id": "255830499",
    "cookies": {
      "sc": "CDA68358-94FB-4D83-9655-3FEB3C4114A3"
    }
  },
  "ProductPage": {
    "Product": {
      "productKey": "72354",
      "price": "754 USD",
      "isInStock": "1",
      "tag_something": [
        "abc",
        "bca"
      ]
    },
    "Tags": {
      "some_tags": [
        "some_tag1",
        "some_tag2"
      ]
    }
  }
}';

        $this->assertEquals($event->toArray(), json_decode($expectedJson, true));
    }
}
