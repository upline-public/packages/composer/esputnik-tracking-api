<?php


use Uplinestudio\EsputnikTrackingApi\DataObjects\CartProduct;
use Uplinestudio\EsputnikTrackingApi\DataObjects\Event;
use Uplinestudio\EsputnikTrackingApi\DataObjects\GeneralInfo;
use Uplinestudio\EsputnikTrackingApi\DataObjects\StatusCart;
use PHPUnit\Framework\TestCase;

class StatusCartTest extends TestCase
{

    public function testToArray()
    {

        $products = [
            (new CartProduct(
                '430738',
                '201.95',
                1
            ))
                ->setDiscount('180')
                ->setPriceCurrencyCode('UAH')
                ->addTagField('something', 'aaa')
                ->addTagField('something', 'bbb'),
            (new CartProduct(
                '902339',
                '596',
                1
            ))
                ->setDiscount('590')
                ->setPriceCurrencyCode('UAH')
                ->addTagField('something', 'aaa')
                ->addTagField('something', 'bbb'),
        ];

        $event = new Event(
            (
            new GeneralInfo(
                '8A412DC',
                1579622183208,
                'CDA68358-94FB-4D83-9655-3FEB3C4114A3'
            )
            )
                ->setUserPhone('1-541-754-3010')
                ->setUserEmail('user@mail.com')
                ->setUserName('Gregori Boczynski')
                ->setUserEsContactId('255830499'),
            (new StatusCart('6F9619FF-8B86-D011-B42D-00CF4FC964FF'))
                ->setProducts($products)
                ->addTag('some_tags', '1')
                ->addTag('some_tags', 'a2')
                ->addTag('some_tags1', '4')
                ->addTag('some_tags1', 'gg')
        );


        $expectedJson = '{
  "GeneralInfo": {
    "eventName": "StatusCart",
    "siteId": "8A412DC",
    "datetime":1579622183208,
    "user_phone": "1-541-754-3010",
    "user_email": "user@mail.com",
    "user_name": "Gregori Boczynski",
    "user_es_contact_id": "255830499",
    "cookies": {
      "sc": "CDA68358-94FB-4D83-9655-3FEB3C4114A3"
    }
  },
  "StatusCart": {
    "GUID": "6F9619FF-8B86-D011-B42D-00CF4FC964FF",
    "Products": [
      {
        "productKey": "430738",
        "price": "201.95",
        "discount": "180",
        "quantity": 1,
        "price_currency_code": "UAH",
        "tag_something": [
          "aaa",
          "bbb"
        ]
      },
      {
        "productKey": "902339",
        "price": "596",
        "discount": "590",
        "quantity": 1,
        "price_currency_code": "UAH",
        "tag_something": [
          "aaa",
          "bbb"
        ]
      }
    ],
    "Tags": {
      "some_tags": [
        "1",
        "a2"
      ],
      "some_tags1": [
        "4",
        "gg"
      ]
    }
  }
}';

        $this->assertEquals($event->toArray(), json_decode($expectedJson, true));
    }
}
