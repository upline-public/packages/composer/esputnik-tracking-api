<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\EsputnikTrackingApi\DataObjects\Event;
use Uplinestudio\EsputnikTrackingApi\DataObjects\GeneralInfo;
use Uplinestudio\EsputnikTrackingApi\DataObjects\ProductPage;
use Uplinestudio\EsputnikTrackingApi\EsputnikTrackingService;

require 'vendor/autoload.php';

$siteId = 'test';

$client = new Client();

$factory = new HttpFactory();

$service = new EsputnikTrackingService(
    $client,
    $factory,
    $factory
);


var_dump($service->registerEvent(new Event(
    new GeneralInfo(
        $siteId,
        time(),
        'test-cookie-value' // $_COOKIES['sc']
    ),
    new ProductPage(
        'test-product-key'
    )
)));
